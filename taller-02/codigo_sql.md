# Creando usuario HR y otorgando privilegios

```sql
CREATE USER HR                           -- Se le asigna un nombre de usuario
IDENTIFIED BY HR                         -- Se le asigna una contraseña (No es necesario usar doble comillas para hacerla case sensitive)
DEFAULT TABLESPACE USERS                 -- En Oracle XE se usa el tablespace USERS para almacenar los objetos de los usuarios
TEMPORARY TABLESPACE TEMP;
 
 -- Asignar un tablespace temporal para los datos temporales de la sesión del usuario
ALTER USER HR QUOTA UNLIMITED ON USERS;  -- Asignar una couta de almacenamiento en el tablespace USERS
GRANT CREATE SESSION TO HR;              -- Privilegio que permite que el usuario se pueda conectar a la BD (Iniciar Sesión)
GRANT CREATE VIEW TO HR;                 -- Privilegio de sistema que permite crear vistas (necesario para ejecutar script hr.sql)
GRANT RESOURCE TO HR;                    -- Asignar el rol "RESOURCE" (permite crear TABLE, SEQUENCE, TRIGGER, PROCEDURE, etc.)
ALTER USER HR DEFAULT ROLE RESOURCE;     -- Se le asigna RESOURCE como el rol por defecto

ALTER USER HR IDENTIFIED BY HR ACCOUNT UNLOCK;

GRANT CREATE SESSION, CREATE TABLE, CREATE VIEW, CREATE PROCEDURE, CREATE SEQUENCE, CREATE TRIGGER TO FRANKITO;

ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY';
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MON-YYYY HH24:MI:SS';
ALTER SESSION SET NLS_DATE_LANGUAGE = 'SPANISH';

GRANT CONNECT TO HR;

GRANT CREATE SESSION, CREATE VIEW, CREATE TABLE, ALTER SESSION, CREATE SEQUENCE TO HR;
GRANT CREATE SYNONYM, CREATE DATABASE LINK, RESOURCE, UNLIMITED TABLESPACE TO HR;


```