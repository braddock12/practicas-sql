# 10. Valores nulos (null)


# Ejercicios propuestos

## Ejercicio 01

Una farmacia guarda información referente a sus medicamentos en una tabla llamada "medicamentos".

1. Elimine la tabla y créela con la siguiente estructura:

```sql
 drop table medicamentos;
 create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);
```

SALIDA
```sh
Table MEDICAMENTOS creado.
```

2. Visualice la estructura de la tabla "medicamentos"
note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".
```sql
DESCRIBE medicamentos;

```
SALIDA
```sh
Nombre      ¿Nulo?   Tipo         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3)
```

3. Ingrese algunos registros con valores "null" para los campos que lo admitan:

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);
```
SALIDA
```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
4. Vea todos los registros.
```sql
SELECT * FROM medicamentos;

```
SALIDA
```sh
CODIGO| NOMBRE|LABORATORIO|PRECIO|CANTIDAD
1	Sertal gotas			100
2	Sertal compuesto		8,9	150
3	Buscapina	Roche		200
```

5. Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.
```sql
INSERT INTO medicamentos (codigo, nombre, precio, laboratorio)
VALUES (1, 'Nombre del Medicamento', 0, NULL);

```
SALIDA
```sh
Error que empieza en la línea: 158 del comando -
INSERT INTO medicamentos (codigo, nombre, precio, laboratorio)
VALUES (1, 'Nombre del Medicamento', 0, NULL)
```

6. Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)
```sql
INSERT INTO medicamentos (codigo, nombre, precio, laboratorio)
VALUES (1, 'Sin nombre', 10.99, 'Laboratorio A');
```
SALIDA
```sh
Error que empieza en la línea: 161 del comando :
INSERT INTO medicamentos (codigo, nombre, precio, laboratorio)
VALUES (1, 'Sin nombre', 10.99, 'Laboratorio A')
Informe de error -
ORA-01400: no se puede realizar una inserción NULL en ("NAYELI"."MEDICAMENTOS"."CANTIDAD")


```

7. Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)
```sql
INSERT INTO medicamentos (codigo, nombre, precio, laboratorio)
VALUES (1, 'Ibuprofeno', 10.99, 'Laboratorio A')
```
SALIDA
```sh
Error que empieza en la línea: 164 del comando :
INSERT INTO medicamentos (codigo, nombre, precio, laboratorio)
VALUES (1, 'Ibuprofeno', 10.99, 'Laboratorio A')
Informe de error -
ORA-01400: no se puede realizar una inserción NULL en ("NAYELI"."MEDICAMENTOS"."CANTIDAD")

```

8. Ingrese un registro con una cadena de 1 espacio para el laboratorio.
```sql
INSERT INTO medicamentos (codigo, nombre, precio, laboratorio)
VALUES (1, 'Paracetamol', 5.99, NULL);
```
SALIDA
```sh
Error que empieza en la línea: 167 del comando :
INSERT INTO medicamentos (codigo, nombre, precio, laboratorio)
VALUES (1, 'Paracetamol', 5.99, NULL)
Informe de error -
ORA-01400: no se puede realizar una inserción NULL en ("NAYELI"."MEDICAMENTOS"."CANTIDAD")

```

9. Recupere los registros cuyo laboratorio contenga 1 espacio (1 registro)
```sql
SELECT * FROM medicamentos WHERE laboratorio = ' ';
```
SALIDA
```sh
CODIGO|NOMBRE| LABORATORIO|PRECIO|CANTIDAD
```

10. Recupere los registros cuyo laboratorio sea distinto de ' '(cadena de 1 espacio) (1 registro)
```sql
```
SALIDA
```sh
CODIGO|NOMBRE| LABORATORIO|PRECIO|CANTIDAD
3	|Buscapina	|Roche	|NULL	|200
```

## Ejercicio 02

Trabaje con la tabla que almacena los datos sobre películas, llamada "peliculas".

1. Elimine la tabla:
```sql
drop table peliculas;
```
SALIDA
```sh
Table PELICULAS borrado.
```

2. Créela con la siguiente estructura:

```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);
```
SALIDA
```sh
Table PELICULAS creado.
```
3. Visualice la estructura de la tabla.


note que el campo "codigo" y "titulo", en la columna "Null" muestran "NOT NULL".

```sql
DESCRIBE peliculas ;
```
SALIDA
```sh
Nombre   ¿Nulo?   Tipo         
-------- -------- ------------ 
CODIGO   NOT NULL NUMBER(4)    
TITULO   NOT NULL VARCHAR2(40) 
ACTOR             VARCHAR2(20) 
DURACION          NUMBER(3)    

```

4. Ingrese los siguientes registros:

```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);
```
SALIDA
```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


```
5. Recupere todos los registros para ver cómo Oracle los almacenó.
```sql

SELECT * FROM peliculas;

```
SALIDA
```sh
CODIGO  |          TITULO          |          ACTOR           | DURACION
----------------------------------------------------------------------
1       | Mision imposible         | Tom Cruise               | 120
2       | Harry Potter y la piedra |                          | 180
        | filosofal               |                          |
3       | Harry Potter y la camara | Daniel R.                |
        | secreta                 |                          |
0       | Mision imposible 2       |                          | 150
4       | Titanic                  | L. Di Caprio             | 220
5       | Mujer bonita             | R. Gere.J. Roberts       | 0


```

6. Intente ingresar un registro con valor nulo para campos que no lo admiten (aparece un mensaje de error)
```sql
INSERT INTO peliculas (codigo, titulo, actor, duracion)
VALUES (6, NULL, NULL, NULL);
```
SALIDA
```sh

Error que empieza en la línea: 193 del comando -
INSERT INTO peliculas (codigo, titulo, actor, duracion)
VALUES (6, NULL, NULL, NULL)
Error en la línea de comandos : 194 Columna : 12
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("NAYELI"."PELICULAS"."TITULO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.

```

7. Muestre todos los registros.
```sql
SELECT * FROM peliculas;
```
SALIDA
```sh
CODIGO | TITULO                                | ACTOR                 | DURACION
-------|---------------------------------------|-----------------------|---------
1      | Mision imposible                       | Tom Cruise            | 120
2      | Harry Potter y la piedra filosofal     | NULL                  | 180
3      | Harry Potter y la camara secreta       | Daniel R.             | NULL
0      | Mision imposible 2                     |                       | 150
4      | Titanic                               | L. Di Caprio          | 220
5      | Mujer bonita                          | R. Gere.J. Roberts    | 0

```

8. Actualice la película en cuyo campo "duracion" hay 0 por "null" (1 registro)
```sql
UPDATE peliculas SET duracion = NULL WHERE duracion = 0;
```
SALIDA
```sh
1 fila actualizadas.
```

9. Recupere todos los registros.
```sql
SELECT * FROM peliculas;

```
SALIDA
```sh
CODIGO  |      TITULO                      |     ACTOR         | DURACION
----------------------------------------------------------------------
   1    |   Mision imposible               |   Tom Cruise      |   120
   2    |   Harry Potter y la piedra...    |     (null)        |   180
   3    |   Harry Potter y la camara...    |   Daniel R.       |  (null)
   0    |   Mision imposible 2             |     (empty)       |   150
   4    |   Titanic                        |   L. Di Caprio    |   220
   5    |   Mujer bonita                   |   R. Gere.J....   |  (null)

```

