# 5. Recuperación de registros específicos (select . where)

# Ejercicios propuestos

## Ejercicio 01

Trabaje con la tabla "agenda" en la que registra los datos de sus amigos.

1. Elimine "agenda"

```sql
DROP TABLE agenda;
```
SALIDA
```sh


Error que empieza en la línea: 19 del comando :
DROP TABLE agenda
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
2. Cree la tabla, con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```

SALIDA
```sh
Table AGENDA creado.
```

3. Visualice la estructura de la tabla "agenda" (4 campos)
```sql
```
SALIDA
```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(30) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 
```

4. Ingrese los siguientes registros ("insert into"):

```sql
insert into agenda(apellido,nombre,domicilio,telefono) values ('Acosta', 'Ana', 'Colon 123', '4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Bustamante', 'Betina', 'Avellaneda 135', '4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Hector', 'Salta 545', '4887788'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Luis', 'Urquiza 333', '4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Marisa', 'Urquiza 333', '4545454');
```
SALIDA
```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

5. Seleccione todos los registros de la tabla (5 registros)
```sql
SELECT * FROM agenda ;
```
SALIDA
```sh
APELLIDO|NOMBRE|DOMICILIO|TELEFONO
Acosta|	Ana|	Colon 123	|4234567
Bustamante	|Betina	|Avellaneda 135|	4458787
Lopez||	Hector|	Salta 545	|4887788
Lopez	Luis|	Urquiza 333	|4545454
Lopez	|Marisa	|Urquiza 333	|4545454
```

6. Seleccione el registro cuyo nombre sea "Marisa" (1 registro)
```sql
SELECT * FROM agenda WHERE nombre = 'Marisa';
```
SALIDA
```sh
APELLIDO|NOMBRE|DOMICILIO|TELEFONO
Lopez	|Marisa	|Urquiza 333	|4545454
```

7. Seleccione los nombres y domicilios de quienes tengan apellido igual a "Lopez" (3 registros)
```sql
SELECT nombre, domicilio FROM agenda WHERE apellido = 'Lopez';
```
SALIDA
```sh
NOMBRE|DOMICILIO
Hector|	Salta 545
Luis|	Urquiza 333
Marisa	|Urquiza 333
```

8. Seleccione los nombres y domicilios de quienes tengan apellido igual a "lopez" (en minúsculas)

No aparece ningún registro, ya que la cadena "Lopez" no es igual a la cadena "lopez".
```sql
SELECT nombre, domicilio FROM agenda WHERE LOWER(apellido) = 'lopez';
```
SALIDA
```sh
NOMBRE|DOMICILIO
Hector|	Salta 545
Luis|	Urquiza 333
Marisa	|Urquiza 333
```

9. Muestre el nombre de quienes tengan el teléfono "4545454" (2 registros)
```sql
SELECT nombre FROM agenda WHERE telefono = '4545454';
```
SALIDA
```sh
NOMBRE
Luis
Marisa
```
## Ejercicio 02

Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla llamada "articulos".

1. Elimine la tabla si existe.
```sql
DROP TABLE articulos;

```
SALIDA
```sh
Table ARTICULOS borrado.
```

2. Cree la tabla "articulos" con la siguiente estructura:

```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2)
);
```
SALIDA
```sh
Table ARTICULOS creado.
```

3. Vea la estructura de la tabla:

```sql
 describe articulos;
```
SALIDA
```sh
Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CODIGO             NUMBER(5)    
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             NUMBER(7,2)  
```
4. Ingrese algunos registros:

```sql
insert into articulos (codigo, nombre, descripcion, precio) values (1,'impresora','Epson Stylus C45',400.80);
insert into articulos (codigo, nombre, descripcion, precio) values (2,'impresora','Epson Stylus C85',500);
insert into articulos (codigo, nombre, descripcion, precio) values (3,'monitor','Samsung 14',800);
insert into articulos (codigo, nombre, descripcion, precio) values (4,'teclado','ingles Biswal',100);
insert into articulos (codigo, nombre, descripcion, precio) values (5,'teclado','español Biswal',90);
```
SALIDA
```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


```
5. Seleccione todos los datos de los registros cuyo nombre sea "impresora" (2 registros)
```sql
SELECT * FROM articulos WHERE nombre = 'impresora';

```
SALIDA
```sh
CODIGO|NOMBRE|DESCRIPCION|PRECIO
1	|impresora	|Epson Stylus C45|	400,8
2	|impresora	|Epson Stylus C85|	500

```

6. Muestre sólo el código, descripción y precio de los teclados (2 registros)
```sql
SELECT codigo, descripcion, precio FROM articulos WHERE nombre = 'teclado';

```
SALIDA
```sh
CODIGO|DESCRIPCION|PRECIO
4	|ingles Biswal	|100
5	|español Biswal|	90
```