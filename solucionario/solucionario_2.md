# 2. Ingresar registros (insert into- select)

## Ejercicios propuestos

### Ejercicio 01

Trabaje con la tabla "agenda" que almacena información de sus amigos.

1. Elimine la tabla "agenda"
```sql
 DROP TABLE agenda;
 ```
SALIDA
 ```sh
 Table AGENDA borrado.
 ```

2. Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11)
```sql
CREATE TABLE agenda (
  apellido VARCHAR(30),
  nombre VARCHAR(20),
  domicilio VARCHAR(30),
  telefono VARCHAR(11)
);
```
SALIDA

```sh
Table AGENDA creado.
```
3. Visualice las tablas existentes para verificar la creación de "agenda" (all_tables)
```sql
SELECT table_name
FROM all_tables
```
SALIDA
```sh
TABLE_NAME
1 AGENDA
2 ALL_UNIFIED_AUDIT_ACTIONS
3 AUDIT_ACTIONS
4 AV_DUAL
```

4. Visualice la estructura de la tabla "agenda" (describe)
```sql
DESCRIBE agenda;
```
SALIDA
```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(20) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 

```

5. Ingrese los siguientes registros:

```sql
insert into agenda (apellido, nombre, domicilio, telefono) values ('Moreno','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');
```
SALIDA
```sh


1 fila insertadas
```

6. Seleccione todos los registros de la tabla.
```sql
SELECT * FROM agenda;
```
SALIDA
```sh
APELLIDO  NOMBRE  DOMICILIO     TELEFONO
Moreno	Alberto	 Colon 123	    4234567
Torres	Juan	Avellaneda 135	4458787
```

7. Elimine la tabla "agenda"
```sql
DROP TABLE agenda;
```
SALIDA
```sh
Table AGENDA borrado.
```

8. Intente eliminar la tabla nuevamente (aparece un mensaje de error)
```sql
DROP TABLE agenda;
```
SALIDA
```sh
Error que empieza en la línea: 41 del comando :
DROP TABLE agenda
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```

### Ejercicio 02

Trabaje con la tabla "libros" que almacena los datos de los libros de su propia biblioteca.

1. Elimine la tabla "libros"
```sql
DROP TABLE libros;
```
SALIDA


```sh
Error que empieza en la línea: 42 del comando :
DROP TABLE libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```

2. Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo (cadena de 20), autor (cadena de 30) y editorial (cadena de 15)

```sql
CREATE TABLE libros (
  titulo VARCHAR(20),
  autor VARCHAR(30),
  editorial VARCHAR(15)
);
```
SALIDA
```sh
Table LIBROS creado.
```
3. Visualice las tablas existentes.
```sql
SELECT table_name
FROM all_tables
```
SALIDA
```sh
67 LIBROS
```
4. Visualice la estructura de la tabla "libros"
Muestra los campos y los tipos de datos de la tabla "libros".
```sql
DESCRIBE libros;
```
SALIDA
```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 
```


5. Ingrese los siguientes registros:

```sql
insert into libros (titulo,autor,editorial) values ('El aleph','Borges','Planeta');
insert into libros (titulo,autor,editorial) values ('Martin Fierro','Jose Hernandez','Emece');
insert into libros (titulo,autor,editorial) values ('Aprenda PHP','Mario Molina','Emece');
```
SALIDA
```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

6. Muestre todos los registros (select) de "libros"
```sql
SELECT * FROM libros
```
SALIDA
```sh
TITULO       AUTOR          EDITORIAL
El aleph	  Borges	     Planeta
Martin Fierro  	Jose Hernandez  Emece
Aprenda PHP	   Mario Molina	      Emece
```