# 3. Tipos de datos

## Ejercicios propuestos

### Ejercicio 01

Un videoclub que alquila películas en video almacena la información de sus películas en una tabla llamada "peliculas"; para cada película necesita los siguientes datos:

```sh
 -nombre, cadena de caracteres de 20 de longitud,
 -actor, cadena de caracteres de 20 de longitud,
 -duración, valor numérico entero que no supera los 3 dígitos.
 -cantidad de copias: valor entero de un sólo dígito (no tienen más de 9 copias de cada película).
```

1. Elimine la tabla "peliculas" si ya existe.
```sql
DROP TABLE peliculas;
```
SALIDA
```sh
Error que empieza en la línea: 57 del comando :
DROP TABLE peliculas
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo.

> Note que los campos "duracion" y "cantidad", que almacenarán valores sin decimales, fueron definidos de maneras diferentes, en el primero especificamos el valor 0 como cantidad de decimales, en el segundo no especificamos cantidad de decimales, es decir, por defecto, asume el valor 0.
```sql
CREATE TABLE peliculas (
  nombre VARCHAR(100),
  actor VARCHAR(100),
  duracion DECIMAL(10, 0),
  cantidad INT
);
```
SALIDA
```sh
Table PELICULAS creado.
```
3. Vea la estructura de la tabla.
```sql
 DESCRIBE peliculas;
```
SALIDA

```sh
Nombre   ¿Nulo? Tipo          
-------- ------ ------------- 
NOMBRE          VARCHAR2(100) 
ACTOR           VARCHAR2(100) 
DURACION        NUMBER(10)    
CANTIDAD        NUMBER(38)    

```

4. Ingrese los siguientes registros:

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',128,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',130,2);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Julia Roberts',118,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',110,2);
```
SALIDA
```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

5. Muestre todos los registros (4 registros)
```sql
SELECT * FROM peliculas;
```
SALIDA
```sh
  NOMBRE    |ACTOR  | DURACION  |CANTIDAD
1 Mision imposible	|Tom Cruise |128	|3
2 Mision imposible 2|	Tom Cruise  |130|2
3 Mujer bonita|	Julia Roberts   	|118|3
4 Elsa y Fred	|China Zorrilla	    |110|2
```
6. Intente ingresar una película con valor de cantidad fuera del rango permitido:

```sql
 insert into peliculas (nombre, actor, duracion, cantidad)
  values ('Mujer bonita','Richard Gere',1200,10);
```
> Mensaje de error
SALIDA
```sh
1 fila insertadas.
```



7. Ingrese un valor con decimales en un nuevo registro, en el campo "duracion":

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Richard Gere',120.20,4);
```
SALIDA
```sh
1 fila insertadas.

```

8. Muestre todos los registros para ver cómo se almacenó el último registro ingresado.
```sql
SELECT * FROM peliculas;
```
SALIDA
```sh
NOMBRE| ACTOR| DURACION | CANTIDAD
Mujer bonita	|Richard Gere|	120|	4
```

9. Intente ingresar un nombre de película que supere los 20 caracteres.
```sql
INSERT INTO peliculas (nombre, actor, duracion, cantidad) VALUES ('Pelicula con nombre muy largo que excede los 20 caracteres', 'John Doe', 120, 1);

```
SALIDA
```sh
1 fila insertadas.
```

### Ejercicio 02

Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico.

1. Elimine la tabla si existe.
```sql
DROP TABLE empleados ;
```
SALIDA
```sh
Error que empieza en la línea: 7 del comando :
DROP TABLE empleados
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```

2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo:

```sql
create table empleados(
    nombre varchar2(20),
    documento varchar2(8),
    sexo varchar2(1),
    domicilio varchar2(30),
    sueldobasico number(6,2)
);
```
SALIDA

```sh
Table EMPLEADOS creado.
```

3. Verifique que la tabla existe consultando

```sql
"all_tables"
```

```sh
```

4. Vea la estructura de la tabla (5 campos)
```sql
DESCRIBE peliculas;
```
SALIDA
```sh

Nombre   ¿Nulo? Tipo          
-------- ------ ------------- 
NOMBRE          VARCHAR2(100) 
ACTOR           VARCHAR2(100) 
DURACION        NUMBER(10)    
CANTIDAD        NUMBER(38)    
```


5. Ingrese algunos registros:

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22333444','m','Sarmiento 123',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24555666','f','Colon 134',650);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolome Barrios','27888999','m','Urquiza 479',800);
```
```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```


6. Seleccione todos los registros (3 registros)
```sql
SELECT * FROM peliculas;
```
SALIDA
```sh

Mujer bonita	Richard Gere	120	4
Esta es una película con un nombre muy largo	John Doe	120	1
Pelicula con nombre muy largo que excede los 20 caracteres	John Doe	120	1
```

7. Intente ingresar un registro con el valor "masculino" en el campo "sexo".

    Un mensaje indica que el campo está definido para almacenar 1 solo caracter como máximo y está intentando ingresar 9 caracteres.
```sql
INSERT INTO peliculas (nombre, actor, duracion, cantidad, sexo) VALUES ('Película', 'John Doe', 120, 1, 'masculino');


```
SALIDA    

```sh 
Error que empieza en la línea: 22 del comando -
INSERT INTO peliculas (nombre, actor, duracion, cantidad, sexo) VALUES ('Película', 'John Doe', 120, 1, 'masculino')
Error en la línea de comandos : 22 Columna : 59
Informe de error -
Error SQL: ORA-00904: "SEXO": identificador no válido
00904. 00000 -  "%s: invalid identifier"
*Cause:    
*Action:
```

8. Intente ingresar un valor fuera de rango, en un nuevo registro, para el campo "sueldobasico"
Mensaje de error.
```sql
INSERT INTO tabla (sueldobasico) VALUES (1500);
```
SALIDA
```sh
Error que empieza en la línea: 23 del comando -
INSERT INTO tabla (sueldobasico) VALUES (1500)
Error en la línea de comandos : 23 Columna : 13
Informe de error -
Error SQL: ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```

9. Elimine la tabla
```sql
DROP TABLE peliculas ;
```
SALIDA
```sh
Table PELICULAS borrado.
```