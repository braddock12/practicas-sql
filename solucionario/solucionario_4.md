# 4 Recuperación de algunos campos 

## Ejercicios laboratorio

---Trabajamos con la tabla "libros" que almacena los datos de los libros de una librería.---

Eliminamos la tabla, si existe:

```sql
DROP TABLE  libros;
```
SALIDA
```sh

Table LIBROS borrado.
```

```sql
drop table if exists libros;
Creamos la tabla "libros":

create table libros(
  titulo varchar(20),
  autor varchar(30),
  editorial varchar(15),
  precio float,
  cantidad integer
);
 ```
 SALIDA
 ```sh
 Table LIBROS creado.

 ```
  



Veamos la estructura de la tabla:

```sql
describe libros;
```
SALIDA
```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 
PRECIO           FLOAT(126)   
CANTIDAD         NUMBER(38)
```   
Ingresamos algunos registros:

```sql
insert into libros (titulo,autor,editorial,precio,cantidad) values ('El aleph','Borges','Emece',45.50,100);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais de las maravillas','Lewis Carroll','Planeta',25,200);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Matematica estas ahi','Paenza','Planeta',15.8,200);
```
 SALIDA
 ```sh
 1 fila insertadas.


1 fila insertadas.

 ```

Para ver todos los campos de una tabla tipeamos:

```sql
select *from libros;
```
 SALIDA
 ```sh
 TITULO| AUTOR|EDITORIAL|PRECIO|CANTIDAD
 Matematica estas ahi|	Paenza|	Planeta|	15,8|	200
 El aleph	|Borges	|Emece|	45,5|	100
 Matematica estas ahi|	Paenza|	Planeta|	15,8	|200
 ```

Con el asterisco (*) indicamos que seleccione todos los campos de la tabla.

Para ver solamente el título, autor y editorial de todos los libros especificamos los nombres de los campos separados por comas:

```sql
select titulo,autor,editorial from libros;
```
 SALIDA
 ```sh
 TITULO| AUTOR|EDITORIAL
 Matematica estas ahi|	Paenza|	Planeta
  El aleph	|Borges	|Emece
   Matematica estas ahi|	Paenza|	Planeta

 ```

La siguiente sentencia nos mostrará los títulos y precios de todos los libros:

```sql
select titulo,precio from libros;
```
 SALIDA
 ```sh
 TITULO|PRECIO
Matematica estas ahi|	15,8
El aleph|	45,5
Matematica estas ahi|	15,8
 ```

Para ver solamente la editorial y la cantidad de libros tipeamos:

```sql
select editorial,cantidad from libros;
```
 SALIDA
 ```sh
 EDITORIAL|CANTIDAD
 Planeta|	200
 Emece	|100
 Planeta	|200

 ```

# Ejercicios propuestos

## Ejercicio 01

1. 

```sql
drop table if exists peliculas;
```
SALIDA
```sh
Error que empieza en la línea: 18 del comando :
drop table if exists peliculas
Informe de error -
ORA-00933: comando SQL no terminado correctamente
00933. 00000 -  "SQL command not properly ended"
*Cause:    
*Action:
```

2. Cree la tabla:

```sql
create table peliculas(
  titulo varchar(20),
  actor varchar(20),
  duracion integer,
  cantidad integer
);
```
SALIDA
```sh
Table PELICULAS creado.
```

3. Vea la estructura de la tabla:

```sql
describe peliculas;
```
SALIDA
```sh
Nombre   ¿Nulo? Tipo         
-------- ------ ------------ 
TITULO          VARCHAR2(20) 
ACTOR           VARCHAR2(20) 
DURACION        NUMBER(38)   
CANTIDAD        NUMBER(38) 
```
4. Ingrese los siguientes registros:

```sql
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,2);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',90,2);
```
SALIDA
```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

5. Realice un "select" mostrando solamente el título y actor de todas las películas:

```sql
select titulo,actor from peliculas;
```
SALIDA
```sh
TITULO | ACTOR
Mision imposible	|Tom Cruise
Mision imposible 2	|Tom Cruise
Mujer bonita|	Julia R.
Elsa y Fred|	China Zorrilla
```
6. Muestre el título y duración de todas las peliculas.
```sql
SELECT titulo, duracion FROM peliculas;


```

SALIDA
```sh
TITULO | DURACION
Mision imposible	|120
Mision imposible 2	|180
Mujer bonita|90
Elsa y Fred|90
```

7. Muestre el título y la cantidad de copias.
```sql
SELECT titulo, cantidad FROM peliculas;

```

SALIDA
```sh
TITULO | CANTIDAD
Mision imposible	|3
Mision imposible 2	|2
Mujer bonita|3
Elsa y Fred|2
```

## Ejercicio 02

A. Una empresa almacena los datos de sus empleados en una tabla llamada "empleados".

1. Elimine la tabla, si existe:

```sql
 drop table if exists empleados;
```
SALIDA
```sh
Error que empieza en la línea: 37 del comando :
drop table if exists empleados
Informe de error -
ORA-00933: comando SQL no terminado correctamente
00933. 00000 -  "SQL command not properly ended"
*Cause:    
*Action:
```

2. Cree la tabla:

```sql
 create table empleados(
  nombre varchar(20),
  documento varchar(8), 
  sexo varchar(1),
  domicilio varchar(30),
  sueldobasico float
 );
```
SALIDA
```sh
Table EMPLEADOS creado.
```

3. Vea la estructura de la tabla:

```sql
describe empleados;
```
SALIDA
```sh
Nombre       ¿Nulo? Tipo         
------------ ------ ------------ 
NOMBRE              VARCHAR2(20) 
DOCUMENTO           VARCHAR2(8)  
SEXO                VARCHAR2(1)  
DOMICILIO           VARCHAR2(30) 
SUELDOBASICO        FLOAT(126)   
```

4. Ingrese algunos registros:

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22345678','m','Sarmiento 123',300);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24345678','f','Colon 134',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Marcos Torres','27345678','m','Urquiza 479',800);
```
```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

5. Muestre todos los datos de los empleados.
```sql
SELECT * FROM empleados;

```

SALIDA
```sh
NOMBRE| DOCUMENTO|SEXO|DOMICILIO|SUELDOBASICO
Juan Perez|	22345678	|m	|Sarmiento 123|	300
Ana Acosta	|24345678|	f	|Colon 134|	500
Marcos Torres|	27345678	|m	|Urquiza 479|	800
```

6. Muestre el nombre y documento de los empleados.
```sql
SELECT nombre, documento FROM empleados;

```

SALIDA
```sh
NOMBRE| DOCUMENTO|
Juan Perez|	22345678	
Ana Acosta	|24345678
Marcos Torres|	27345678
```

7. Realice un "select" mostrando el nombre, documento y sueldo básico de todos los empleados.
```sql
SELECT nombre, documento, sueldo_basico FROM empleados;


```

SALIDA
```sh
NOMBRE| DOCUMENTO|SUELDOBASICO
Juan Perez|	22345678	|	300
Ana Acosta	|24345678|	500
Marcos Torres|	27345678	|	800
```



 ## B) Un comercio que vende artículos de computación registra la información de sus productos en la tabla llamada "articulos".

1. Elimine la tabla si existe:

 drop table if exists articulos;
 ```sql
DROP TABLE  articulos;
```

SALIDA
```sh
Error que empieza en la línea: 61 del comando :
DROP TABLE  articulos
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```

2. Cree la tabla "articulos" con los campos necesarios para almacenar los siguientes datos:

* código del artículo: entero,
* nombre del artículo: 20 caracteres de longitud,
* descripción: 30 caracteres de longitud,
* precio: float.
```sql
CREATE TABLE articulos (
    codigo_articulo INT,
    nombre_articulo VARCHAR(20),
    descripcion VARCHAR(30),
    precio FLOAT
);

```

SALIDA
```sh
Table ARTICULOS creado.
```

3. Vea la estructura de la tabla (describe).
```sql
DESCRIBE articulos;
```

SALIDA
```sh
Nombre          ¿Nulo? Tipo         
--------------- ------ ------------ 
CODIGO_ARTICULO        NUMBER(38)   
NOMBRE_ARTICULO        VARCHAR2(20) 
DESCRIPCION            VARCHAR2(30) 
PRECIO                 FLOAT(126)
```
4. Ingrese algunos registros:

```sql
 insert into articulos (codigo, nombre, descripcion, precio)
  values (1,'impresora','Epson Stylus C45',400.80);
 insert into articulos (codigo, nombre, descripcion, precio)
  values (2,'impresora','Epson Stylus C85',500);
 insert into articulos (codigo, nombre, descripcion, precio)
  values (3,'monitor','Samsung 14',800);
```


SALIDA
```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

5. Muestre todos los campos de todos los registros.
```sql
SELECT * FROM articulos;
```

SALIDA
```sh
CODIGO| NOMBRE| DESCRIPCION |PRECIO
1|	impresora|	Epson Stylus C45	|400,8
2	|impresora||	Epson Stylus C85	|500
3	|monitor	Samsung 14|	800
```

6. Muestre sólo el nombre, descripción y precio.
```sql
SELECT nombre, descripcion, precio FROM articulos;
```

SALIDA
```sh
 NOMBRE| DESCRIPCION |PRECIO
	impresora|	Epson Stylus C45	|400,8
impresora||	Epson Stylus C85	|500
monitor	Samsung 14|	800
```