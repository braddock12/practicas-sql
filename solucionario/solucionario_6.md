# 6. Operadores relacionales

# Ejercicios propuesto

## Ejercicio 01

Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.

1. Elimine "articulos"

```sql
DROP TABLE articulos;

```
SALIDA
```sh
Table ARTICULOS borrado.

```

2. Cree la tabla, con la siguiente estructura:

```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(6,2),
    cantidad number(3)
);
 ```
 SALIDA
```sh
Table ARTICULOS creado.
```

3. Vea la estructura de la tabla.

```sql
 describe articulos;
```
SALIDA
```sh

Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CODIGO             NUMBER(5)    
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             NUMBER(6,2)  
CANTIDAD           NUMBER(3)    

```

4. Ingrese algunos registros:

```sql
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);
```
SALIDA
```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```

5. Seleccione los datos de las impresoras (2 registros)
```sql
SELECT * FROM articulos WHERE nombre = 'impresora';
```
SALIDA
```sh
CODIGO|NOMBRE|DESCRIPCION|PRECIO
1	|impresora	|Epson Stylus C45|	400,8
2	|impresora	|Epson Stylus C85|	500
```

6. Seleccione los artículos cuyo precio sea mayor o igual a 400 (3 registros)
```sql
SELECT * FROM articulos WHERE precio >= 400;
```
SALIDA
```sh
CODIGO|NOMBRE| DESCRIPCION|PRECIO|CANTIDAD
1	|impresora	|Epson Stylus C45|	400,8	|20
2	|impresora	|Epson Stylus C85|	500	|30
3	|monitor	|Samsung 14	|800	|10
```

7. Seleccione el código y nombre de los artículos cuya cantidad sea menor a 30 (2 registros)
```sql
SELECT codigo, nombre FROM articulos WHERE cantidad < 30;
```
SALIDA
```sh
CODIGO|NOMBRE
1|	impresora
3	|monitor

```

8. Selecciones el nombre y descripción de los artículos que NO cuesten $100 (4 registros)
```sql
SELECT nombre, descripcion FROM articulos WHERE precio <> 100;

```
SALIDA
```sh
NOMBRE| DESCRIPCION|PRECIO|CANTIDAD
impresora	|Epson Stylus C45
impresora	|Epson Stylus C85
monitor	|Samsung 14	
teclado	|español Biswal

```

## Ejercicio 02

Un video club que alquila películas en video almacena la información de sus películas en alquiler en una tabla denominada "peliculas".

1. Elimine la tabla.

```sql
drop table peliculas;
```
SALIDA
```sh
Table PELICULAS borrado.

```

2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo:

```sql
create table peliculas(
    titulo varchar2(20),
    actor varchar2(20),
    duracion number(3),
    cantidad number(1)
);
 ```

 SALIDA
```sh
Table PELICULAS creado.
```

3. Ingrese los siguientes registros:

```sql
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,4);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,1);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',80,2);
```
SALIDA
```sh

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

4. Seleccione las películas cuya duración no supere los 90 minutos (2 registros)
```sql
SELECT * FROM peliculas WHERE duracion <= 90;
```
SALIDA
```sh
TITULO|ACTOR|DURACION|CANTIDAD
Mujer bonita|	Julia R.	|90|	1
Elsa y Fred	|China Zorrilla	|80|	2
```
5. Seleccione el título de todas las películas en las que el actor NO sea "Tom Cruise" (2 registros)
```sql
SELECT titulo FROM peliculas WHERE actor <> 'Tom Cruise';
```
SALIDA
```sh
  TITULO
1 Mujer bonita
2 Elsa y Fred

```
6. Muestre todos los campos, excepto "duracion", de todas las películas de las que haya más de 2 copias (2 registros)
```sql
SELECT titulo, actor, cantidad, precio FROM peliculas WHERE cantidad > 2;

```
SALIDA
```sh
ORA-00904: "PRECIO": identificador no válido
00904. 00000 -  "%s: invalid identifier"
*Cause:    
*Action:
Error en la línea: 63, columna: 
```