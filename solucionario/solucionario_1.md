# 1. Crear tablas (create table - describe - all_tables - drop table)

## Ejercicios propuestos

#### Ejercicio 01

Necesita almacenar los datos de amigos en una tabla.
Los datos que guardará serán: apellido, nombre, domicilio y teléfono.

1. Elimine la tabla "agenda"
Si no existe, un mensaje indicará tal situación.
```sql

DROP TABLE agenda;
```
salida
```sh
[2023-06-01 14:10:03] [42000][942] ORA-00942: la tabla o vista no existe
[2023-06-01 14:10:03] Position: 11
```


2. Intente crear una tabla llamada "*agenda"

```sql
create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```

>Aparece un mensaje de error indicando que usamos un caracter inválido ("*") para el nombre de la tabla.
```sh
NAYELI> create table*agenda(
            apellido varchar2(30),
            nombre varchar2(20),
            domicilio varchar2(30),
            telefono varchar2(11)
        )
[2023-06-01 14:14:46] [42000][903] ORA-00903: nombre de tabla no válido
[2023-06-01 14:14:46] Position: 12

```


3. Cree una tabla llamada "agenda", debe tener los siguientes campos: apellido, varchar2(30); nombre, varchar2(20); domicilio, varchar2 (30) y telefono, varchar2(11)

> Un mensaje indica que la tabla ha sido creada exitosamente.
```sql
create table agenda (
  apellido VARCHAR2(30),
  nombre VARCHAR2(20),
  domicilio VARCHAR2(30),
  telefono VARCHAR2(11)
);
```
salida
```sh

Table AGENDA creado.
```

4. Intente crearla nuevamente.
Aparece mensaje de error indicando que el nombre ya lo tiene otro objeto.

salida
```sh
Error que empieza en la línea: 1 del comando :
create table agenda (
  apellido VARCHAR2(30),
  nombre VARCHAR2(20),
  domicilio VARCHAR2(30),
  telefono VARCHAR2(11)
)
Informe de error -
ORA-00955: este nombre ya lo está utilizando otro objeto existente
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:
salida
```

5. Visualice las tablas existentes (all_tables)
La tabla "agenda" aparece en la lista.

```sql
SELECT table_name
FROM all_tables ;
```
salida
```sh
TABLE_NAME
AGENDA
ALL_UNIFIED_AUDIT_ACTIONS
AUDIT_ACTIONS
AV_DUAL
AW$AWCREATE
AW$AWCREATE10G
AW$AWMD
AW$AWREPORT
AW$AWXML
AW$EXPRESS
DATA_PUMP_XPL_TABLE$
DR$NUMBER_SEQUENCE
DR$OBJECT_ATTRIBUTE
DR$POLICY_TAB
DR$THS
DR$THS_PHRASE
DUAL
FINALHIST$
HELP
IMPDP_STATS
KU$_DATAPUMP_MASTER_10_1
KU$_DATAPUMP_MASTER_11_1
KU$_DATAPUMP_MASTER_11_1_0_7
KU$_DATAPUMP_MASTER_11_2
KU$_DATAPUMP_MASTER_12_0
KU$_DATAPUMP_MASTER_12_2
KU$_LIST_FILTER_TEMP
KU$_LIST_FILTER_TEMP_2
KU$NOEXP_TAB
KU$_SHARD_DOMIDX_NAMEMAP
KU$XKTFBUE
LIBROS
MODELGTTRAW$
NTV2_XML_DATA
ODCI_PMO_ROWIDS$
ODCI_SECOBJ$
ODCI_WARNINGS$
OGIS_GEOMETRY_COLUMNS
OGIS_SPATIAL_REFERENCE_SYSTEMS
OL$

```


6. Visualice la estructura de la tabla "agenda" (describe)
   Aparece la siguiente tabla:

```sql
Name Null Type
-----------------------

APELLIDO VARCHAR2(30)
NOMBRE  VARCHAR2(20)
DOMICILIO VARCHAR2(30)
TELEFONO VARCHAR2(11)
```

CODIGO
```sql
DESCRIBE agenda;
```
SALIDA
```sH
Error que empieza en la línea: 10 del comando :
 all_tables
Informe de error -
Comando desconocido

Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(20) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 

```


### Ejercicio 02

Necesita almacenar información referente a los libros de su biblioteca personal. Los datos que guardará serán: título del libro, nombre del autor y nombre de la editorial.

1. Elimine la tabla "libros"
Si no existe, un mensaje indica tal situación.

```sql
DROP TABLE agenda;

```
SALIDA
```sh
Error que empieza en la línea: 11 del comando :
DROP TABLE libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```

2. Verifique que la tabla "libros" no existe (all_tables)
No aparece en la lista.
```sql
SELECT table_name
FROM all_tables

```
SALIDA
```sh
TABLE_NAME
AGENDA
ALL_UNIFIED_AUDIT_ACTIONS
AUDIT_ACTIONS
AV_DUAL
AW$AWCREATE
AW$AWCREATE10G
AW$AWMD
AW$AWREPORT
AW$AWXML
AW$EXPRESS
DATA_PUMP_XPL_TABLE$
DR$NUMBER_SEQUENCE
DR$OBJECT_ATTRIBUTE
DR$POLICY_TAB
DR$THS
DR$THS_PHRASE
DUAL
FINALHIST$
HELP
IMPDP_STATS
KU$_DATAPUMP_MASTER_10_1
KU$_DATAPUMP_MASTER_11_1
KU$_DATAPUMP_MASTER_11_1_0_7
KU$_DATAPUMP_MASTER_11_2
KU$_DATAPUMP_MASTER_12_0
KU$_DATAPUMP_MASTER_12_2
KU$_LIST_FILTER_TEMP
KU$_LIST_FILTER_TEMP_2
KU$NOEXP_TAB
KU$_SHARD_DOMIDX_NAMEMAP
KU$XKTFBUE
MODELGTTRAW$
NTV2_XML_DATA
ODCI_PMO_ROWIDS$
ODCI_SECOBJ$
ODCI_WARNINGS$
OGIS_GEOMETRY_COLUMNS
OGIS_SPATIAL_REFERENCE_SYSTEMS
OL$
OL$HINTS
OL$NODES
PLAN_TABLE$
RDF_PARAMETER
SAM_SPARSITY_ADVICE
SCHEDULER_FILEWATCHER_QT
SDO_COORD_AXES
SDO_COORD_AXIS_NAMES
SDO_COORD_OP_METHODS
SDO_COORD_OP_PARAMS
SDO_COORD_OP_PARAM_USE

```

3. Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo, varchar2(20); autor, varchar2(30) y editorial, varchar2(15)

```sql
CREATE TABLE libros (
  titulo VARCHAR2(20),
  autor VARCHAR2(30),
  editorial VARCHAR2(15)
);
```
SALIDA

```sh
Table LIBROS creado.
```


4. Intente crearla nuevamente:
Aparece mensaje de error indicando que existe un objeto con el nombre "libros".
```sql
CREATE TABLE libros (
  titulo VARCHAR2(20),
  autor VARCHAR2(30),
  editorial VARCHAR2(15)
);
```
SALIDA
```sh
Error que empieza en la línea: 15 del comando :
CREATE TABLE libros (
  titulo VARCHAR2(20),
  autor VARCHAR2(30),
  editorial VARCHAR2(15)
)
Informe de error -
ORA-00955: este nombre ya lo está utilizando otro objeto existente
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:
```
5. Visualice las tablas existentes
```sql
SELECT table_name
FROM all_tables;

```
SALIDA
```sh
1 AGENDA
32 LIBROS
```
6. Visualice la estructura de la tabla "libros":
Aparece "libros" en la lista.
```sql
DESCRIBE libros;
```
SALIDA
```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 
```
7. Elimine la tabla
```sql
DROP TABLE libros;
```
SALIDA
```sh
Table LIBROS borrado.
```

8. Intente eliminar la tabla
Un mensaje indica que no existe.
```sql

DROP TABLE libros;
```
SALIDA

```sh


Error que empieza en la línea: 25 del comando :
DROP TABLE libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```


